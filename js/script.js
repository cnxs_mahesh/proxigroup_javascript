$(document).ready(function(){
  $("#filter").select2({
      placeholder: "Select Areas",
      width: '100%'
  })

  var table=$("#records").DataTable({
            "bAutoWidth": true,
            stateSave: true,
            responsive: true,
            'info': false
          });

  $('#from_date').daterangepicker({
        startDate: moment(),
        autoUpdateInput: false,
    },
    function(start, end, label) {
        $('#from_date').val(start.format('YYYY-MM-DD'));
        $('#to_date').val(end.format('YYYY-MM-DD'));
    });

    $('#duration').timeDurationPicker({
        defaultValue: function() {
          return $('#seconds').val();
        },
        onSelect: function(element, seconds, duration) {
          $('#seconds').val(seconds);
          $('#duration').val(duration);
        }
      });

    /*$(document).on('click', '.search', function(event) {
      var formObj = $("#formdata");
      var formData = new FormData(formObj[0]);
    })
*/



})
